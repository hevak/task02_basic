package com.epam.edu.online;
import java.util.Scanner;
/**
 * This class run project functional
 *
 * @author Oleh Hevak
 */
public class Main {
    /**
     * The main() method is the entry point into the application.
     *
     * @param args is an array of strings which stores arguments
     *             passed by command line while starting a program
     */
    public static void main(final String[] args) {
        System.out.println("Please, Enter initial data!");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Minimum value: ");
        final int minCount = scanner.nextInt();
        System.out.print("Maximum value: ");
        final int maxCount = scanner.nextInt();
        System.out.print("Size of the Fibonacci number list: ");
        final int sizeListNumber = scanner.nextInt();


        CountNumber.printOddNumbers(minCount, maxCount);
        CountNumber.printOddNumbersReversOrder(minCount, maxCount);
        CountNumber.printSummaryOddNumbers(minCount, maxCount);
        CountNumber.printSummaryEvenNumbers(minCount, maxCount);
        CountNumber.printMaxEvenNumberFibonacci(sizeListNumber);
        CountNumber.printMaxOddNumberFibonacci(sizeListNumber);
        CountNumber.printPercentageOddAndEvenFibonacci(sizeListNumber);
    }
}
