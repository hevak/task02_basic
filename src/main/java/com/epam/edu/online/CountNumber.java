package com.epam.edu.online;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * This class consists exclusively of methods that operate
 * initial data to print some result
 *
 * @author Oleh Hevak
 */
public class CountNumber {

    private CountNumber() {
    }

    /**
     * Tuning parameters for algorithms even numbers
     */
    private static final int EVEN_NUMBER = 0;
    /**
     * Tuning parameters for algorithms odd numbers
     */
    private static final int ODD_NUMBER = 1;

    /**
     * This method print odd numbers based on the specified interval
     *
     * @param minCount value to start the interval
     * @param maxCount value to even the interval
     */
    public static void printOddNumbers(int minCount, final int maxCount) {
        System.out.println("Print odd numbers by asc");
        while (minCount <= maxCount) {
            if (minCount % 2 == ODD_NUMBER) {
                System.out.print(minCount + " ");
            }
            minCount++;
        }
        System.out.println();
    }

    /**
     * This method print odd numbers based on the specified
     * interval in reverse order
     *
     * @param minCount value to start the interval
     * @param maxCount value to even the interval
     */
    public static void printOddNumbersReversOrder(final int minCount, int maxCount) {
        System.out.println("Print odd numbers by desc");
        while (maxCount >= minCount) {
            if (maxCount % 2 == ODD_NUMBER) {
                System.out.print(maxCount + " ");
            }
            maxCount--;
        }
        System.out.println();
    }

    /**
     * This method prints the sum of odd numbers based
     * on the specified interval
     *
     * @param minCount value to start the interval
     * @param maxCount value to even the interval
     */
    public static void printSummaryOddNumbers(final int minCount, final int maxCount) {
        printSummaryNumbers(minCount, maxCount, ODD_NUMBER);
    }

    /**
     * This method prints the sum of even numbers based
     * on the specified interval
     *
     * @param minCount value to start the interval
     * @param maxCount value to even the interval
     */
    public static void printSummaryEvenNumbers(final int minCount, final int maxCount) {
        printSummaryNumbers(minCount, maxCount, EVEN_NUMBER);
    }

    private static void printSummaryNumbers(int minCount, final int maxCount, final int i) {
        int summary = 0;
        while (minCount <= maxCount) {
            if (minCount % 2 == i) {
                summary += minCount;
            }
            minCount++;
        }
        System.out.print("Summary "
                + (i == 0 ? "even" : "odd")
                + " numbers: " + summary + " ");
        System.out.println();
    }

    /**
     * This method prints the greatest of even number
     * from list Fibonacci numbers.
     * The list capacity based on the input value
     *
     * @param sizeListNumber the capacity of the list of Fibonacci numbers
     */
    public static void printMaxEvenNumberFibonacci(final int sizeListNumber) {
        System.out.println("The largest even number: " + getMaxNumberFibonacci(sizeListNumber, EVEN_NUMBER));
    }

    /**
     * This method prints the greatest of odd number
     * from list Fibonacci numbers
     * The list capacity based on the input value
     *
     * @param sizeListNumber the capacity of the list of Fibonacci numbers
     */
    public static void printMaxOddNumberFibonacci(final int sizeListNumber) {
        System.out.println("The largest odd number: " + getMaxNumberFibonacci(sizeListNumber, ODD_NUMBER));
    }


    private static int getMaxNumberFibonacci(final int sizeListNumber, final int i) {
        List<Integer> listFibonacci = getListFibonacciByReversOrder(sizeListNumber);
        for (Integer number : listFibonacci) {
            if (number % 2 == i) {
                return number;
            }
        }
        return 0;
    }

    private static List<Integer> getListFibonacciByReversOrder(final int sizeListNumber) {
        List<Integer> fibonacci = new ArrayList<>();
        int firstNumber = 0;
        int secondNumber = 1;
        for (int i = 0; i < sizeListNumber; i++) {
            fibonacci.add(firstNumber);
            int newNextNumber = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = newNextNumber;
        }
        fibonacci.sort(Comparator.reverseOrder());
        return fibonacci;
    }

    /**
     *
     * This method prints the percentage of odd and even numbers
     * from list Fibonacci numbers
     * The list capacity based on the input value
     *
     * @param sizeListNumber the capacity of the list of Fibonacci numbers
     */
    public static void printPercentageOddAndEvenFibonacci(final int sizeListNumber) {
        float countEvenNumber = 0;
        for (Integer number : getListFibonacciByReversOrder(sizeListNumber)) {
            if (number % 2 == EVEN_NUMBER) {
                countEvenNumber++;
            }
        }
        /**
         * Tuning parameters for algorithms even and odd numbers
         */
        int fullPercentage = 100;
        double evenPercentage = countEvenNumber / sizeListNumber * fullPercentage;
        System.out.println("Even numbers: " + evenPercentage + "%");
        System.out.println("Odd numbers: " + (fullPercentage - evenPercentage) + "%");
    }


}
